import {
  randEmail,
  randFullName,
  randFirstName,
  randLastName,
} from "@ngneat/falso";
import { createRequire } from "module";
const require = createRequire(import.meta.url);
var Airtable = require("airtable");
Airtable.configure({
  endpointUrl: "https://api.airtable.com",
  apiKey: "key3OASHzEhJVQm3c",
});
var base = Airtable.base("appdyEDY7p29nKFpZ");
let generateData = (count) => {
  for (let i = 0; i < count; i++) {
    let firstName = randFirstName();
    let lastName = randLastName();
    let fullName = firstName + " " + lastName;
    let email = randEmail({
      firstName: firstName,
      lastName: lastName,
      nameSeparator: ".",
      provider: "gmail",
      suffix: "com",
    });
    base("SOURCE TABLE").create(
      [
        {
          fields: {
            "FULL NAME": fullName,
            "FIRST NAME": firstName,
            "LAST NAME": lastName,
            EMAIL: email,
            STATUS: "Active",
          },
        },
      ],
      function (err, records) {
        if (err) {
          console.error(err);
          return;
        }
        records.forEach(function (record) {
          console.log(record.getId());
        });
      }
    );
    console.log({
      "FIRST NAME": firstName,
      "LAST NAME": lastName,
      "FULL NAME": fullName,
      EMAIL: email,
    });
  }
};
generateData(10);
