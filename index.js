require("dotenv").config();
var airtable = require("airtable");
var Airtable = require("airtable");
airtable.configure({
  endpointUrl: "https://api.airtable.com",
  apiKey: process.env.SOURCE_API_KEY,
});
Airtable.configure({
  endpointUrl: "https://api.airtable.com",
  apiKey: process.env.DESTINATION_API_KEY,
});
const employeeDestBase = Airtable.base(process.env.DESTINATION_BASE_ID);
const employeeDestTable = employeeDestBase(process.env.DESTINATION_TABLE_ID);
const employeeSrcBase = airtable.base(process.env.SOURCE_BASE_ID);
const employeeSrcTable = employeeSrcBase(process.env.SOURCE_TABLE_ID);
const employeesUpdate = async () => {
  try {
    const fetchRequirements = await employeeSrcTable.select().all();
    const fetchReq = await employeeDestTable.select().all();
    console.log(fetchReq.length);
    if (fetchRequirements) {
      console.info("All data fetched successfully");
    }
    //console.log(fetchRequirements);
    if (fetchReq.length == 0) {
      for (let i = 0; i < fetchRequirements.length; i++) {
        console.log(fetchRequirements[i].Description);
        const createNewReq = await employeeDestTable.create([
          {
            fields: {
              "FULL NAME": fetchRequirements[i].fields["FULL NAME"],
              "FIRST NAME": fetchRequirements[i].fields["FIRST NAME"],
              "LAST NAME": fetchRequirements[i].fields["LAST NAME"],
              EMAIL: fetchRequirements[i].fields["EMAIL"],
              STATUS: fetchRequirements[i].fields["STATUS"],
              ForeginId: fetchRequirements[i].id,
            },
          },
        ]);
      }
    } else {
      for (i = 0; i < fetchRequirements.length; i++) {
        const fetch = await employeeDestTable
          .select({
            fields: [
              "FULL NAME",
              "FIRST NAME",
              "LAST NAME",
              "EMAIL",
              "STATUS",
              "ForeginId",
            ],
            filterByFormula: `{ForeginId}='${fetchRequirements[i].id}'`,
          })
          .all();

        if (fetch[0] != undefined) {
          const confirmMatch =
            fetch[0].fields.ForeginId == fetchRequirements[i].id;

          if (confirmMatch) {
            console.log("Employee already exists--- update");
          } else {
            console.log("Create ");
            const createNewReq = await employeeDestTable.create([
              {
                fields: {
                  "FULL NAME": fetchRequirements[i].fields["FULL NAME"],
                  "FIRST NAME": fetchRequirements[i].fields["FIRST NAME"],
                  "LAST NAME": fetchRequirements[i].fields["LAST NAME"],
                  EMAIL: fetchRequirements[i].fields["EMAIL"],
                  STATUS: fetchRequirements[i].fields["STATUS"],
                  ForeginId: fetchRequirements[i].id,
                },
              },
            ]);
          }
        } else {
          console.log("create record");
          const createNewReq = await employeeDestTable.create([
            {
              fields: {
                "FULL NAME": fetchRequirements[i].fields["FULL NAME"],
                "FIRST NAME": fetchRequirements[i].fields["FIRST NAME"],
                "LAST NAME": fetchRequirements[i].fields["LAST NAME"],
                EMAIL: fetchRequirements[i].fields["EMAIL"],
                STATUS: fetchRequirements[i].fields["STATUS"],
                ForeginId: fetchRequirements[i].id,
              },
            },
          ]);
        }
      }
    }
  } catch (err) {
    console.log(err);
  }
};
employeesUpdate();
